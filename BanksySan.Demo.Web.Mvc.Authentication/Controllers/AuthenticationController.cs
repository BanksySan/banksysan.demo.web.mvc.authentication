﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Controllers
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using DataStore;
    using Models;
    using Newtonsoft.Json;

    public class AuthenticationController : Controller
    {
        private readonly FakeDataStore _dataStore = new FakeDataStore();

        [HttpPost]
        public ActionResult LogIn(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user =
                    _dataStore.Users
                        .FirstOrDefault(u => u.UserName == model.Username && u.Password == model.Password);

                if (user != null)
                {
                    var roleNames = user.Roles.Select(role => role.Name).ToArray();

                    var serializeModel = new CustomPrincipalSerializeModel
                    {
                        UserName = user.UserName,
                        Roles = roleNames
                    };

                    var userData = JsonConvert.SerializeObject(serializeModel);
                    var issueDate = DateTime.Now;

                    var authenticationTicket = new FormsAuthenticationTicket(1,
                        user.UserName,
                        issueDate,
                        issueDate.AddMinutes(5),
                        false,
                        userData);

                    var encryptedTicket = FormsAuthentication.Encrypt(authenticationTicket);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    Response.AppendCookie(cookie);

                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Error", model);
        }

        public ActionResult Error(string message)
        {
            TempData["message"] = message;
            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}