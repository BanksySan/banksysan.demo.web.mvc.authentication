﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Controllers
{
    using System.Web.Mvc;
    using Authentication;

    [CustomAuthorize(Roles = "User")]
    public class UserOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}