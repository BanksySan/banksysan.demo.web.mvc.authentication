﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Web.Security;
    using DataStore;
    using Models;

    public class HomeController : Controller
    {
        private static readonly FakeDataStore DATA_STORE = new FakeDataStore();

        public ActionResult Index(bool expireCookie = false)
        {
            if (expireCookie)
            {
                FormsAuthentication.SignOut();
                //var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];

                //if (cookie != null)
                //{
                //    cookie.Expires = DateTime.Now.AddMinutes(-1);
                //}
            }

            var homeModel = new HomeModel
            {
                Roles = DATA_STORE.Roles,
                Users = DATA_STORE.Users
            };
            return View(homeModel);
        }
    }
}