﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Controllers
{
    using System.Web.Mvc;
    using Authentication;

    [CustomAuthorize(Roles = "Administrator")]
    public class AdministratorOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}