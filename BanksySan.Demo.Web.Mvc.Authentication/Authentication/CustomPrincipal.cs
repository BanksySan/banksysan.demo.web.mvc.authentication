﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Authentication
{
    using System.Linq;
    using System.Security.Principal;

    public class CustomPrincipal : IPrincipal
    {
        /// <summary>
        ///     Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <returns>
        ///     true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        /// <param name="requiredRoleRaw">The name of the role for which to check membership. </param>
        public bool IsInRole(string role)
        {
            if (Roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public CustomPrincipal(string Username)
        {
            Identity = new GenericIdentity(Username);
        }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string[] Roles { get; set; }

        /// <summary>
        ///     Gets the identity of the current principal.
        /// </summary>
        /// <returns>
        ///     The <see cref="T:System.Security.Principal.IIdentity" /> object associated with the current principal.
        /// </returns>
        public IIdentity Identity { get; }
    }
}