﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Authentication
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected virtual CustomPrincipal CurrentUser => HttpContext.Current.User as CustomPrincipal;

        /// <summary>
        ///     Called when a process requests authorization.
        /// </summary>
        /// <param name="filterContext">
        ///     The filter context, which encapsulates information for using
        ///     <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="filterContext" /> parameter is null.</exception>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                var userInRole = CurrentUser.IsInRole(Roles);

                if (! userInRole)
                {
                    filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new { Controller = "Authentication", Action = "Error", Message = "User not in role" }));
                }
            }
            else
            {
                filterContext.Result =
                    new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                Controller = "Authentication",
                                Action = "Error",
                                Message = "User is not authenticated"
                            }));
            }
        }
    }
}