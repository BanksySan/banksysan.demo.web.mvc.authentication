﻿namespace BanksySan.Demo.Web.Mvc.Authentication
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using System.Web.Security;
    using Authentication;
    using DataStore;
    using Models;
    using Newtonsoft.Json;

    public class MvcApplication : HttpApplication
    {
        private static readonly FakeDataStore DATA_STORE = new FakeDataStore();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (sender == null)
            {
                throw new ArgumentNullException(nameof(sender));
            }

            var authenticationCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authenticationCookie != null)
            {
                var authenticationTicket = FormsAuthentication.Decrypt(authenticationCookie.Value);

                var serializedModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authenticationTicket.UserData,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });

                var principle = new CustomPrincipal(authenticationTicket.Name)
                {
                    UserId = 1,
                    FirstName = "unknown-first-name",
                    LastName = "unknown-last-name",
                    Roles = DATA_STORE.Roles.Where(x => serializedModel.Roles.Contains(x.Name)).Select(x => x.Name).ToArray()
                };

                HttpContext.Current.User = principle;
            }
        }
    }
}