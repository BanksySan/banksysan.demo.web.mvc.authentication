﻿namespace BanksySan.Demo.Web.Mvc.Authentication.DataStore
{
    using System.Collections.Generic;

    internal class FakeDataStore
    {
        public FakeDataStore()
        {
            var adminRole = new Role { Name = "Administrator" };
            var userRole = new Role { Name = "User" };

            var alice = new User { Password = "alice-password", UserName = "alice" };
            var bob = new User { Password = "bob-password", UserName = "bob" };
            var charlie = new User { Password = "charlie-password", UserName = "charlie" };

            alice.Roles = new[] { adminRole };
            bob.Roles = new[] { adminRole, userRole };
            charlie.Roles = new[] { userRole };

            adminRole.Users = new[] { alice, bob };
            userRole.Users = new[] { bob, charlie };

            Users = new[] { alice, bob, charlie };
            Roles = new[] { adminRole, userRole };
        }

        public IEnumerable<User> Users { get; }

        public IEnumerable<Role> Roles { get; }
    }
}