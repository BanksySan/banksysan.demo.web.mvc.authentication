﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Models
{
    using System.Collections.Generic;
    using DataStore;

    public class HomeModel
    {
        public IEnumerable<User> Users { get; set; }

        public IEnumerable<Role> Roles { get; set; }
    }
}