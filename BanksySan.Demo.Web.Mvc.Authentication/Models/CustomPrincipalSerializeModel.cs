﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Models
{
    using System.Collections.Generic;

    public class CustomPrincipalSerializeModel
    {
        public string UserName { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}