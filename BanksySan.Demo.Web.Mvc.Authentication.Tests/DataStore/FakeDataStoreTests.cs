﻿namespace BanksySan.Demo.Web.Mvc.Authentication.Tests.DataStore
{
    using System.Linq;
    using Mvc.Authentication.DataStore;
    using NUnit.Framework;

    [TestFixture]
    public class FakeDataStoreTests
    {
        private readonly FakeDataStore _fakeDataStore = new FakeDataStore();

        [Test]
        public void UsersAndRolesAreConfiguredConsistently()
        {
            var users = _fakeDataStore.Users.ToArray();
            var roles = _fakeDataStore.Roles.ToArray();

            foreach (var user in users)
            {
                foreach (var role in roles)
                {
                    var userHasRole = user.Roles.Contains(role);
                    var roleHasUser = role.Users.Contains(user);

                    Assert.That(userHasRole,
                        Is.EqualTo(roleHasUser),
                        string.Format(
                            $"role.Users.Contains({user.UserName}) == {role.Users.Contains(user)}, but user.Roles.Contains({role.Name}) == {user.Roles.Contains(role)}"));
                }
            }
        }
    }
}